#!/bin/bash

pwd=$(pwd)
cd /proc/sys/net/bridge
for f in bridge-nf-*; do echo 0 > $f; done

for i in $(seq 1 16); do
	sudo brctl addbr br$i
	sudo ifconfig br$i 192.168.$i.254 up
done

cd $pwd
