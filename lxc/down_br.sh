#!/bin/bash

for i in $(seq 1 16); do
	sudo ifconfig br$i down
	sudo brctl delbr br$i
done
