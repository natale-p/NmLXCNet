#!/bin/bash

if [[ $1 -eq "-h" ]]; then
	echo -en "Usage:\n\t$0"
	exit
fi

sudo ./nmlxcnet_test -i reth -f rx -b linux
