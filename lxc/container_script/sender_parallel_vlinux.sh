#!/bin/bash

if [[ $1 -eq "-h" ]]; then
	echo -en "Usage:\n$0 [number of send if] [packets] [delay]"
fi

if=""
for i in $(seq 1 $1); do
	if="$if -i seth$i"
done

sudo ./nmlxcnet_test $if -f tx -b linux -R $3 -n $2
