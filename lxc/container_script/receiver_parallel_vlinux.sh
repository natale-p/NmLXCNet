#!/bin/bash

if [[ $1 -eq "-h" || $# -ne 1 ]]; then
	echo -en "Usage:\n\t$0 [number of recv if]"
	exit
fi

if=""
for i in $(seq 9 $(($1+8))); do
	if="$if -i reth$i"
done

sudo ./nmlxcnet_test $if -f rx -b linux
