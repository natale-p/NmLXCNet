
# $1 sender
# $2 receiver
# $3 count

if [[ $1 -eq "-h" || $# -ne 3 ]]; then
	echo -en "Usage:\n\t$0 [from] [to] [count]"
	exit
fi

echo -en "vale$1:2 ---> vale$2:1\n"
sudo ./nmlxcnet_test -i vale$1:2 -o vale$2:1 -b netmap -f fw -n $3

