
# $1 fwd number

if [[ $1 -eq "-h" || $# -ne 1 ]]; then
	echo -en "Usage:\n\t$0 [fwd self number]"
	exit
fi

n=$(($1+$1))

sudo ./nmlxcnet_test -i vale$(($n-1)):2 -o vale$n:1 -b netmap -f fw

