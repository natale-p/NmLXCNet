
# $1 receiver
# $2 count

if [[ $1 -eq "-h" || $# -ne 2 ]]; then
	echo -en "Usage:\n\t$0 [receiver] [count]"
	exit
fi


echo -en "Sending on vale$1:1\n"
sudo ./nmlxcnet_test -i vale$1:1 -b netmap -f tx -n $2
