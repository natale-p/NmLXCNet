
# $1 sender
# $2 receiver
# $3 count

if [[ $1 -eq "-h" || $# -ne 3 ]]; then
	echo -en "Usage:\n\t$0 [from] [to] [count]"
	exit
fi

MAC_SRC=00:00:00:00:$1:01
MAC_DST=00:00:00:00:$2:02

IP_SRC=192.168.$1.1
IP_DST=192.168.$(($2-1)).2

sudo ./nmlxcnet_test -i reth -o seth -b linux -f fw -S $MAC_SRC \
	-D $MAC_DST -s $IP_SRC -d $IP_DST -n $3

