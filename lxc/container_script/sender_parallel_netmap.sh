#!/bin/bash

if [[ $1 -eq "-h" ]]; then
	echo -en "Usage:\n$0 [number of send if] [packets] [delay]"
fi

if=""
for i in $(seq 1 2 $(($1*2))); do
	if="$if -i vale$i:1"
done

sudo ./nmlxcnet_test $if -f tx -b netmap -R $3 -n $2
