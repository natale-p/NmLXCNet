#!/bin/bash

if [[ $1 -eq "-h" || $# -ne 1 ]]; then
	echo -en "Usage:\n\t$0 [receiver]"
	exit
fi

echo -en "Waiting on vale$1:2\n"
sudo ./nmlxcnet_test -i vale$1:2 -f rx -b netmap
