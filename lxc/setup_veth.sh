#!/bin/bash


echo 1 > /proc/sys/net/ipv4/ip_forward

for i in $(seq 1 16); do
	ip link add name seth$i type veth peer name reth$i
	ifconfig seth$i 192.168.$i.1 up
	ifconfig reth$i 192.168.$i.2 up

	echo 1 > /proc/sys/net/ipv4/conf/seth$i/proxy_arp
	echo 1 > /proc/sys/net/ipv4/conf/reth$i/proxy_arp
done
