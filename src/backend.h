#include <config.h>

#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <netinet/udp.h>
#include <netinet/ether.h>      /* ether_aton */
#include <linux/if_packet.h>    /* sockaddr_ll */

extern int debug;

#define ND(fmt, args...) do {} while (0)
#define D1(fmt, args...) do {} while (0)
#define D(fmt, args...) fprintf(stderr, "%-8s " fmt "\n",      \
              __FUNCTION__, ## args)
#define DX(lev, fmt, args...) do {                             \
              if (debug > lev) D(fmt, ## args); } while (0)

struct pkt {
        //uint8_t padding[4];
        struct ether_header eh;
        struct ip ip;
        struct udphdr udp;
        uint8_t body[2048];     // XXX hardwired
} __attribute__((__packed__));

struct ip_range {
        char *name;
        struct in_addr start, end;
};

struct mac_range {
        char *name;
        struct ether_addr start, end;
};

enum open_mod {
	OPEN_TX = 2,
	OPEN_RX = 4,
};

struct open_arg_t {
	char *dev_name;
	enum open_mod flag;
};

struct fun_ret_t {
	uint64_t count;
	uint64_t ns;
	int pkt_size;
};

struct tx_arg_t {
	int pkt_size;
	int pkt_in_burst;
	struct pkt *master_pkt;
	uint64_t count;
	struct timespec *rem;
	struct fun_ret_t *ret;
	void *state;
};

struct rx_arg_t {
	uint64_t count;
	int pkt_in_burst;
	struct fun_ret_t *ret;
	void *state;
};

struct fw_arg_t {
	char *tx_dev_name;
	uint64_t count;
	struct pkt *master_pkt;
	int pkt_size;
	int pkt_in_burst;
	struct timespec *rem;
	struct fun_ret_t *tx_ret;
	struct fun_ret_t *rx_ret;
	void *state;
};

struct backend_t {
	char *name;
	int (*open)(struct open_arg_t*, void *);
	int (*close)(void *);
	int (*tx)(struct tx_arg_t*);
	int (*rx)(struct rx_arg_t*, int (*)(int, void*));
	void (*debug)(void*);
	char* (*getcore)();
};

struct stats_t {
	uint64_t ns;
	int size;
	uint64_t count;
	double pps;
	char pps_mult;
	double bw;
	char bw_mult;
};
