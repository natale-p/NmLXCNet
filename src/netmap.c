#include <netmap.h>
#include <netmap_user.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <poll.h>
#include <signal.h>

#include "gticks.h"
#include "backend.h"

int netmap_open(struct open_arg_t*, void*);
int netmap_close(void*);
int netmap_tx(struct tx_arg_t*);
int netmap_rx(struct rx_arg_t*, int (*)(int, void*));
void netmap_debug_state(void*);
char* netmap_getcore();

extern void debug_tx(struct pkt*);

struct backend_t netmap_backend = {
	"netmap",
	netmap_open,
	netmap_close,
	netmap_tx,
	netmap_rx,
	netmap_debug_state,
	netmap_getcore
};

struct netmap_priv_core {
	int fd;
	char *devname;
	char *mem;                      /* userspace mmap address */
        uint32_t memsize;
	uint32_t if_flags;
	uint32_t begin, end;
	uint32_t queueid;
	struct netmap_if *nifp;
        struct netmap_ring *tx, *rx;    /* shortcuts */
};

char* netmap_getcore()
{
	char *core = malloc(sizeof (struct netmap_priv_core));
	return core;
}

static int nm_do_ioctl(struct netmap_priv_core *me, int what, int subcmd)
{
	struct ifreq ifr;
	struct ethtool_value eval;
	int error, fd;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0) {
		D("Error: cannot get device control socket.");
		return -1;
	}

	(void)subcmd;	// unused

	bzero(&ifr, sizeof(ifr));
	strncpy(ifr.ifr_name, me->devname, sizeof(ifr.ifr_name));

	switch (what) {
	case SIOCSIFFLAGS:
		ifr.ifr_flags = me->if_flags & 0xffff;
		break;

	case SIOCETHTOOL:
		eval.cmd = subcmd;
		eval.data = 0;
		ifr.ifr_data = (caddr_t)&eval;
		break;
	}

	error = ioctl(fd, what, &ifr);

	if (error)
		goto done;

	switch (what) {
	case SIOCGIFFLAGS:
		D("flags are 0x%x", me->if_flags);
		break;

	}
done:
	close(fd);
	//if (error)
		//D("ioctl error %d %d", error, what);
	return error;
}

int netmap_open(struct open_arg_t *arg, void *state)
{
	struct nmreq req;
	struct netmap_priv_core *core = (struct netmap_priv_core *) state;
	int err, l;

	core->devname = arg->dev_name;
	core->fd = open("/dev/netmap", O_RDWR);

	if (core->fd < 0) {
		D("Unable to open /dev/netmap");
		return -1;
	}

	bzero(&req, sizeof(req));
	req.nr_version = NETMAP_API;
	req.nr_ringid = 0;

	strncpy(req.nr_name, arg->dev_name, sizeof(req.nr_name));

	err = ioctl(core->fd, NIOCGINFO, &req);

	if (err) {
		D("cannot get info on %s, errno %d ver %d",
			arg->dev_name, errno, req.nr_version);
		goto error;
	}

	core->memsize = l = req.nr_memsize;

	//D("memsize is %d MB", l>>20);

	err = ioctl(core->fd, NIOCREGIF, &req);

	if (err) {
		D("Unable to register %s", arg->dev_name);
		goto error;
	}

	core->mem = mmap(0, l, PROT_WRITE | PROT_READ, MAP_SHARED, core->fd, 0);

	if (core->mem == MAP_FAILED) {
		D("Unable to mmap");
		core->mem = NULL;
		goto error;
	}

	/* Set the operating mode. */
	if (0 != NETMAP_SW_RING) {
		nm_do_ioctl(core, SIOCGIFFLAGS, 0);

		if ((core->if_flags & IFF_UP) == 0) {
			D("%s is down, bringing up...", core->devname);
			core->if_flags |= IFF_UP;
		}
		//if (arg->promisc) {
		if (0) {
			core->if_flags |= IFF_PROMISC;
			nm_do_ioctl(core, SIOCSIFFLAGS, 0);

			nm_do_ioctl(core, SIOCGIFFLAGS, 0);
			core->if_flags |= IFF_PROMISC;
			nm_do_ioctl(core, SIOCSIFFLAGS, 0);
		}

		/* disable:
		 * - generic-segmentation-offload
		 * - tcp-segmentation-offload
		 * - rx-checksumming
		 * - tx-checksumming
		 * XXX check how to set back the caps.
		 */
		nm_do_ioctl(core, SIOCETHTOOL, ETHTOOL_SGSO);
		nm_do_ioctl(core, SIOCETHTOOL, ETHTOOL_STSO);
		nm_do_ioctl(core, SIOCETHTOOL, ETHTOOL_SRXCSUM);
		nm_do_ioctl(core, SIOCETHTOOL, ETHTOOL_STXCSUM);
	}

	core->nifp = NETMAP_IF(core->mem, req.nr_offset);
	core->queueid = 0;

	if (core->nifp == NULL) {
		D("Null if pointer O_o");
		return -1;
	}

	if (0 & NETMAP_SW_RING) {
		core->begin = req.nr_rx_rings;
		core->end = core->begin + 1;
		core->tx = NETMAP_TXRING(core->nifp, req.nr_tx_rings);
		core->rx = NETMAP_RXRING(core->nifp, req.nr_rx_rings);
	} else if (0 & NETMAP_HW_RING) {
		D("XXX check multiple threads");
		core->begin = 0 & NETMAP_RING_MASK;
		core->end = core->begin + 1;
		core->tx = NETMAP_TXRING(core->nifp, core->begin);
		core->rx = NETMAP_RXRING(core->nifp, core->begin);
	} else {
		core->begin = 0;
		core->end = req.nr_rx_rings; // XXX max of the two
		core->tx = NETMAP_TXRING(core->nifp, 0);
		core->rx = NETMAP_RXRING(core->nifp, 0);
	}

	D("Wait 2 secs for phy reset");
	sleep(2);
	return 0;
error:
	ioctl(core->fd, NIOCUNREGIF, NULL);
	close(core->fd);
	return -1;
}

int netmap_close(void *state)
{
	struct netmap_priv_core *core = (struct netmap_priv_core *) state;
	if (core->mem)
		munmap(core->mem, core->memsize);
	ioctl(core->fd, NIOCUNREGIF, NULL);
	close(core->fd);
	return 0;
}

static int
receive_packets(struct netmap_ring *ring, uint32_t limit, int *len,
		int (*callback)(int, void*), void *state)
{
	uint32_t cur, rx;

	cur = ring->cur;
	if (ring->avail < limit)
		limit = ring->avail;
	for (rx = 0; rx < limit; rx++) {
		struct netmap_slot *slot = &ring->slot[cur];
		char *p = NETMAP_BUF(ring, slot->buf_idx);
		*len = slot->len;
		(void)p;
//		if (!skip_payload)
//			check_payload(p, slot->len);

		cur = NETMAP_RING_NEXT(ring, cur);
	}
	if (callback != NULL)
		callback(limit, state);
	ring->avail -= rx;
	ring->cur = cur;

	return (rx);
}

void netmap_debug_state(void* state)
{
	struct netmap_priv_core *core = (struct netmap_priv_core *) state;

	D("Devname: %s", core->devname);
}

int netmap_rx (struct rx_arg_t *arg, int (*callback)(int, void*))
{
	struct fun_ret_t *ret = arg->ret;
	struct netmap_priv_core *core = (struct netmap_priv_core *) arg->state;
	struct pollfd fds[1];
	struct netmap_if *nifp = core->nifp;
	struct netmap_ring *rxring;
	int len = 0, i;
	uint64_t received = 0;
	uint64_t wstart = 0, start, end;

	ret->count = 0;

	/* setup poll(2) mechanism. */
	memset(fds, 0, sizeof(fds));
	fds[0].fd = core->fd;
	fds[0].events = (POLLIN);

	/* unbounded wait for the first packet. */
	for (;;) {
		i = poll(fds, 1, 1000);
		if (i > 0 && !(fds[0].revents & POLLERR))
			break;
		//D("waiting for initial packets, poll returns %d %d", i, fds[0].revents);
	}

	/*if (targ->g->use_pcap) {
	for (;;) {
		pcap_dispatch(targ->g->p, targ->g->burst, receive_pcap, NULL);
	}
	} else {*/
	start = gticks_serial();

	while (arg->count == 0 || received < arg->count) {
		/* Once we started to receive packets, wait at most 1 seconds
		   before quitting. */
		wstart = gticks_serial();
		if (poll(fds, 1, 1 * 1000) <= 0) {
			break;
		}

		for (i = 0; i < 1; i++) {
			int m;

			rxring = NETMAP_RXRING(nifp, i);
			if (rxring->avail == 0)
				continue;

			m = receive_packets(rxring, arg->pkt_in_burst, &len, callback, arg->state);
			received += m;

		}

		// tell the card we have read the data
		//ioctl(fds[0].fd, NIOCRXSYNC, NULL);
	}
	end = gticks_serial();

	ret->ns = cyc2ns(end-start) - cyc2ns(end-wstart);
	ret->pkt_size = len;
	ret->count = received;

	return 0;
}

#define MTU 1500

/*
 * create and enqueue a batch of packets on a ring.
 * On the last one set NS_REPORT to tell the driver to generate
 * an interrupt when done.
 */
static int
send_packets(struct netmap_ring *ring, struct pkt *pkt,
		int size, uint32_t count)
{
	u_int sent, cur = ring->cur;

	if (ring->avail < count)
		count = ring->avail;

#if 0
	if (options & (OPT_COPY | OPT_PREFETCH) ) {
		for (sent = 0; sent < count; sent++) {
			struct netmap_slot *slot = &ring->slot[cur];
			char *p = NETMAP_BUF(ring, slot->buf_idx);

			prefetch(p);
			cur = NETMAP_RING_NEXT(ring, cur);
		}
		cur = ring->cur;
	}
#endif
	for (sent = 0; sent < count; sent++) {
		struct netmap_slot *slot = &ring->slot[cur];
		char *p = NETMAP_BUF(ring, slot->buf_idx);

/*		if (options & OPT_COPY)
			pkt_copy(pkt, p, size);
		else if (options & OPT_MEMCPY)
			memcpy(p, pkt, size);
		else if (options & OPT_PREFETCH)
			prefetch(p);
*/
		memcpy(p, pkt, size);
		slot->len = size;
		if (sent == count - 1)
			slot->flags |= NS_REPORT;
		cur = NETMAP_RING_NEXT(ring, cur);
	}
	ring->avail -= sent;
	ring->cur = cur;

	return (sent);
}

int netmap_tx_raw(struct tx_arg_t *arg, struct fun_ret_t *ret, void *state)
{
	struct netmap_priv_core *core = (struct netmap_priv_core *) state;

	struct pollfd fds[1];
	struct netmap_if *nifp = core->nifp;
	struct netmap_ring *txring;
	int sent = 0, i;
	uint64_t start, end ;

	/* setup poll(2) mechanism. */
	memset(fds, 0, sizeof(fds));
	fds[0].fd = core->fd;
	fds[0].events = (POLLOUT);

	start = gticks_serial();
	while (arg->count == 0 || sent < arg->count) {
		/*
		 * wait for available room in the send queue(s)
		 */
		if (poll(fds, 1, 2000) <= 0) {
			D("poll error/timeout on queue \n");
			return -1;
		}
		/*
		 * scan our queues and send on those with room
		 */
		/*if (options & OPT_COPY && sent > 100000 && !(targ->g->options & OPT_COPY) ) {
			D("drop copy");
			options &= ~OPT_COPY;
		}*/
		for (i = 0; i < 1; i++) {
			int m, limit = arg->pkt_in_burst;
			if (arg->count > 0 && arg->count - sent < limit)
				limit = arg->count - sent;
			txring = NETMAP_TXRING(nifp, i);
			if (txring->avail == 0)
				continue;
			m = send_packets(txring, arg->master_pkt, arg->pkt_size,
					 limit);

			sent += m;
		}
	}
	/* flush any remaining packets */
	ioctl(fds[0].fd, NIOCTXSYNC, NULL);

	/* final part: wait all the TX queues to be empty. */
	for (i = 0; i < 1; i++) {
		txring = NETMAP_TXRING(nifp, i);
		while (!NETMAP_TX_RING_EMPTY(txring)) {
			ioctl(fds[0].fd, NIOCTXSYNC, NULL);
			usleep(1); /* wait 1 tick */
		}
	}
	end = gticks_serial();

	ret->ns = cyc2ns(end-start);
	ret->pkt_size = arg->pkt_size;
	ret->count = sent;
	return 0;
}

/* Remember: this is a thread, or it will lock until first packet */
int netmap_tx(struct tx_arg_t *arg)
{
	struct netmap_priv_core *core = (struct netmap_priv_core *) arg->state;
	struct fun_ret_t *ret = arg->ret;

	struct pollfd fds[1];
	struct netmap_if *nifp = core->nifp;
	struct netmap_ring *txring;
	int sent = 0, i;
	uint64_t start, end, send, sstart = 0, sleep_ns;
	long fiftyperc, skew=0;

	struct timespec delay;
	delay.tv_sec = 0;
	if (arg->rem == NULL || arg->rem->tv_nsec == 0)
		return netmap_tx_raw(arg, ret, arg->state);

	delay.tv_nsec = arg->rem->tv_nsec;

	/* setup poll(2) mechanism. */
	memset(fds, 0, sizeof(fds));
	fds[0].fd = core->fd;
	fds[0].events = (POLLOUT);

	start = gticks_serial();
	while (arg->count == 0 || sent < arg->count) {
		/*
		 * wait for available room in the send queue(s)
		 */
		if (poll(fds, 1, 2000) <= 0) {
			D("poll error/timeout on queue \n");
			return -1;
		}
		/*
		 * scan our queues and send on those with room
		 */
		/*if (options & OPT_COPY && sent > 100000 && !(targ->g->options & OPT_COPY) ) {
			D("drop copy");
			options &= ~OPT_COPY;
		}*/
		for (i = 0; i < 1; i++) {
			int m, limit = arg->pkt_in_burst;
			if (arg->count > 0 && arg->count - sent < limit)
				limit = arg->count - sent;
			txring = NETMAP_TXRING(nifp, i);
			if (txring->avail == 0)
				continue;
			m = send_packets(txring, arg->master_pkt, arg->pkt_size,
					 limit);

			sent += m;
			sstart = gticks_serial();
			nanosleep(&delay, NULL);
			send = gticks_serial();

			sleep_ns = cyc2ns(send - sstart);
			skew = sleep_ns - arg->rem->tv_nsec;
			fiftyperc = abs(skew * 0.5);

			if (skew > 0)
				delay.tv_nsec -= fiftyperc;
			else
				delay.tv_nsec += fiftyperc;
		}
	}
	/* flush any remaining packets */
	ioctl(fds[0].fd, NIOCTXSYNC, NULL);

	/* final part: wait all the TX queues to be empty. */
	for (i = 0; i < 1; i++) {
		txring = NETMAP_TXRING(nifp, i);
		while (!NETMAP_TX_RING_EMPTY(txring)) {
			ioctl(fds[0].fd, NIOCTXSYNC, NULL);
			usleep(1); /* wait 1 tick */
		}
	}
	end = gticks_serial();

	ret->ns = cyc2ns(end-start);
	ret->pkt_size = arg->pkt_size;
	ret->count = sent;
	return 0;
}


