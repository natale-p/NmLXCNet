#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/poll.h> /* pollfd and poll */
#include <signal.h>
#include <unistd.h> /* SIGALRM*/
#include <sched.h>

#include "gticks.h"
#include "backend.h"

int linux_open(struct open_arg_t*, void*);
int linux_close(void*);
int linux_tx(struct tx_arg_t*);
int linux_rx(struct rx_arg_t*, int (*)(int, void*));
void linux_debug_state(void*);
char* linux_getcore();

extern void debug_tx(struct pkt*);

struct backend_t linux_backend = {
	"linux",
	linux_open,
	linux_close,
	linux_tx,
	linux_rx,
	linux_debug_state,
	linux_getcore
};

struct linux_priv_core {
	int sockfd;
	struct ifreq *if_idx;
	char *name;
};

char *linux_getcore()
{
	char *core = malloc(sizeof(struct linux_priv_core));
	return core;
}

void linux_debug_state(void * state)
{
	(void)state;
}

int linux_open(struct open_arg_t *arg, void *state)
{
	struct linux_priv_core *core = (struct linux_priv_core*) state;
	core->if_idx = malloc(sizeof(struct ifreq));

	struct ifreq if_mac;
	struct sockaddr_ll sll;

	/* Open RAW socket to send on */
	if ((core->sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1) {
		D("Can't open the socket");
		return -1;
	}

	memset(core->if_idx, 0, sizeof(struct ifreq));
	strncpy(core->if_idx->ifr_name, arg->dev_name, IFNAMSIZ-1);
	core->name = arg->dev_name;

	if (ioctl(core->sockfd, SIOCGIFINDEX, core->if_idx) < 0) {
		D("Error on SIOCGIFINDEX");
		return -1;
	}

	if (arg->flag == OPEN_RX) {
		memset(&sll, 0, sizeof(sll));
		sll.sll_family = AF_PACKET;
		sll.sll_ifindex = core->if_idx->ifr_ifindex;
		sll.sll_protocol = htons(ETH_P_ALL);

		if ((core->if_idx->ifr_flags | IFF_UP | IFF_BROADCAST | IFF_RUNNING) != core->if_idx->ifr_flags) {
			core->if_idx->ifr_flags |= IFF_UP | IFF_BROADCAST | IFF_RUNNING;
			if( ioctl( core->sockfd, SIOCSIFFLAGS, core->if_idx ) < 0 ) {
				perror("ioctl(SIOCSIFFLAGS) failed");
				return 1;
			}
		}

		if (bind(core->sockfd, (struct sockaddr *) &sll, sizeof(sll)) < 0) {
			perror("bind(ETH_P_ALL) failed");
			return 1;
		}
	}

	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, arg->dev_name, IFNAMSIZ-1);

	if (ioctl(core->sockfd, SIOCGIFHWADDR, &if_mac) < 0) {
		D("SIOCGIFHWADDR");
		return -1;
	}

	return 0;
}

int linux_close(void *state)
{
	struct linux_priv_core *core = (struct linux_priv_core*) state;

	D("Closing fd for %s...", core->name);
	close(core->sockfd);

	free(core->if_idx);
	core->if_idx = NULL;

	return 0;
}

static int linux_tx_raw(int burst, struct tx_arg_t *arg, struct fun_ret_t *ret,
		struct linux_priv_core *core, struct sockaddr_ll *socket)
{
	int i, j;

	uint64_t start, end, ticks=0;
	for (i=0; i<burst; i++) {
		sched_yield();
		start = gticks_serial();
		for (j=0; j<arg->pkt_in_burst; j++) {
			sendto(core->sockfd, arg->master_pkt, arg->pkt_size, 0,
				(struct sockaddr*)socket, sizeof(struct sockaddr_ll));
		}
		end = gticks_serial();
		ticks += end-start;
	}


	ret->count = arg->count;
	ret->ns = cyc2ns(ticks);
	ret->pkt_size = arg->pkt_size;
	return 0;
}

int linux_tx(struct tx_arg_t *arg)
{
	int i, j, burst = arg->count/arg->pkt_in_burst;

	struct sockaddr_ll socket_address;
	struct linux_priv_core *core = (struct linux_priv_core*) arg->state;
	struct fun_ret_t *ret = arg->ret;

	/* Index of the network device */
	socket_address.sll_ifindex = core->if_idx->ifr_ifindex;

	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;

	/* Destination MAC */
	bcopy(arg->master_pkt->eh.ether_dhost, &socket_address.sll_addr, 6);
	//D("SEND this pack");
	//debug_tx(arg->master_pkt);

	if (arg->rem == NULL || arg->rem->tv_nsec == 0)
		return linux_tx_raw(burst, arg, ret, core, &socket_address);

	/* Send packet */
	uint64_t start, end, sleep_end, ticks = 0, sleep_ns;
	long fiftyperc, skew = 0;
	struct timespec delay;
	delay.tv_sec = 0;
	delay.tv_nsec = arg->rem->tv_nsec;

	// activate the receiver
	sendto(core->sockfd, arg->master_pkt, arg->pkt_size, 0,
		(struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll));

	for (i=0; i<burst-1; i++) {
		sched_yield();
		start = gticks_serial();
		nanosleep(&delay, NULL);
		sleep_end = gticks_serial();
		for (j=0; j<arg->pkt_in_burst; j++) {
			sendto(core->sockfd, arg->master_pkt, arg->pkt_size, 0,
				(struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll));
		}
		end = gticks_serial();

		ticks += end-start;
		sleep_ns = cyc2ns(sleep_end - start);

		skew = sleep_ns - arg->rem->tv_nsec;
		fiftyperc = abs(skew * 0.5);
		//D("Dormito per %"PRIu64" us ma dovevo per %li us", sleep_ns/1000, arg->rem->tv_nsec/1000);
		//D("Ho accumulato un ritardo di %li", skew/1000);

		if (skew > 0)
			delay.tv_nsec -= fiftyperc;
		else
			delay.tv_nsec += fiftyperc;
		//D("50%%: %li us, dormo per %li us", fiftyperc/1000, delay.tv_nsec/1000);
	}

	sched_yield();
	start = gticks_serial();
	nanosleep(&delay, NULL);

	// one is already sent at the beginning to activate the receiver
	for (j=1; j<arg->pkt_in_burst; j++) {
		sendto(core->sockfd, arg->master_pkt, arg->pkt_size, 0,
			(struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll));
	}

	// Send the remaining from the modulo
	for (j=0; j < arg->count % arg->pkt_in_burst; j++) {
		sendto(core->sockfd, arg->master_pkt, arg->pkt_size, 0,
			(struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll));
	}
	end = gticks_serial();
	ticks += end-start;

	ret->count = arg->count;
	ret->ns = cyc2ns(ticks);
	ret->pkt_size = arg->pkt_size;

	return 0;
}

#define MTU 1500

/* Remember: this is a thread, or it will lock until first packet */
int linux_rx (struct rx_arg_t *arg, int (*callback)(int, void*))
{
	struct fun_ret_t *ret = arg->ret;
	struct pollfd fds[1];

	struct linux_priv_core *core = (struct linux_priv_core*) arg->state;
	struct sockaddr from;
	socklen_t fromlen = sizeof(struct sockaddr_in);
	int i, now_read = 0, B_read = 0, p_read = 0;
	char buffer[MTU];

	memset(fds, 0, sizeof(fds));
	fds[0].fd = core->sockfd;
	fds[0].events = (POLLIN);

	uint64_t start, end, wstart;
	/* unbounded wait for the first packet. */
	for (;;) {
		i = poll(fds, 1, 1000);
		if (i > 0 && !(fds[0].revents & POLLERR))
			break;
		//D("waiting for initial packets, poll returns %d %d\n", i, fds[0].revents);
	}

	// activate other end if we are fw
	if (callback != NULL)
		callback(1, arg->state);

	start = gticks_serial();
	while(1) {
		//sched_yield();
		/* Note that "buffer" should be at least the MTU size of the interface, eg 1500 bytes */
		wstart = gticks_serial();
		if (poll(fds, 1, 1 * 1000) <= 0)
			break;

		now_read = recvfrom(core->sockfd, buffer, sizeof(buffer), 0,(struct sockaddr *)&from,&fromlen);
		//now_read = recv(core->sockfd, buffer, sizeof(buffer), 0);

		//D("Recv this pack:");
		//debug_tx((struct pkt*) buffer);

		if (now_read < 0)
			break;

		++p_read;
		B_read += now_read;
		if (callback != NULL && now_read > 0 && p_read % arg->pkt_in_burst == 0)
			callback(arg->pkt_in_burst, arg->state);

		if (arg->count > 0 && p_read > arg->count)
			break;
	}
	end = gticks_serial();

	if (callback != NULL)
		callback ((p_read % arg->pkt_in_burst)-1, arg->state);

	ret->count = p_read;
	ret->ns = cyc2ns(end-start) - cyc2ns(end-wstart);
	if (p_read != 0)
		ret->pkt_size = (B_read / p_read) ;
	else
		ret->pkt_size = 0;

	return 0;
}

