#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <inttypes.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>

#include "backend.h"
#include "gticks.h"

extern struct backend_t linux_backend;
extern struct backend_t netmap_backend;

const char *default_payload="natmap pkt-gen Luigi Rizzo and Matteo Landi\n"
        "http://info.iet.unipi.it/~luigi/netmap/ ";

/* Compute the checksum of the given ip header. */
static uint16_t checksum(const void *data, uint16_t len, uint32_t sum)
{
        const uint8_t *addr = data;
        uint32_t i;

        /* Checksum all the pairs of bytes first... */
        for (i = 0; i < (len & ~1U); i += 2) {
                sum += (u_int16_t)ntohs(*((u_int16_t *)(addr + i)));
                if (sum > 0xFFFF)
                        sum -= 0xFFFF;
        }
        /*
         * If there's a single byte left over, checksum it, too.
         * Network byte order is big-endian, so the remaining byte is
         * the high byte.
         */
        if (i < len) {
                sum += addr[i] << 8;
                if (sum > 0xFFFF)
                        sum -= 0xFFFF;
        }
        return sum;
}

static u_int16_t wrapsum(u_int32_t sum)
{
        sum = ~sum & 0xFFFF;
        return (htons(sum));
}

static inline int min(int a, int b) { return a < b ? a : b; }

/*
 * Fill a packet with some payload.
 * We create a UDP packet so the payload starts at
 *      14+20+8 = 42 bytes.
 */
#ifdef __linux__
#define uh_sport source
#define uh_dport dest
#define uh_ulen len
#define uh_sum check
#endif /* linux */
static int initialize_packet(struct pkt *pkt, int pkt_size, char *src_ip,
		char *dst_ip, char *src_mac, char *dst_mac)
{
	struct ether_header *eh;
	struct ip *ip;
	struct udphdr *udp;

	uint16_t paylen = pkt_size - sizeof(*eh) - sizeof(struct ip);
	int i, l, l0 = strlen(default_payload);

        for (i = 0; i < paylen;) {
                l = min(l0, paylen - i);
                bcopy(default_payload, pkt->body + i, l);
                i += l;
        }
        pkt->body[i-1] = '\0';
        ip = &pkt->ip;

        ip->ip_v = IPVERSION;
        ip->ip_hl = 5;
        ip->ip_id = 0;
        ip->ip_tos = IPTOS_LOWDELAY;
        ip->ip_len = ntohs(pkt_size - sizeof(*eh));
        ip->ip_id = 0;
        ip->ip_off = htons(IP_DF); /* Don't fragment */
        ip->ip_ttl = IPDEFTTL;
        ip->ip_p = IPPROTO_UDP;
	if (dst_ip != NULL) {
		if (inet_aton(dst_ip, &(ip->ip_dst)) == 0) {
			D("%s isn't a valid IP address.", dst_ip);
			return -1;
		}
	} else
		inet_aton("192.168.0.2", &(ip->ip_dst));

	if (src_ip != NULL) {
		if (inet_aton(src_ip, &(ip->ip_src)) == 0) {
			D("%s isn't a valid IP address.", src_ip);
			return -1;
		}
	} else
		inet_aton("192.168.0.1", &(ip->ip_src));
        ip->ip_sum = wrapsum(checksum(ip, sizeof(*ip), 0));


        udp = &pkt->udp;
        udp->uh_sport = htons(4096);
        udp->uh_dport = htons(8192);
        udp->uh_ulen = htons(paylen);
        /* Magic: taken from sbin/dhclient/packet.c */
        udp->uh_sum = wrapsum(checksum(udp, sizeof(*udp),
                    checksum(pkt->body,
                        paylen - sizeof(*udp),
                        checksum(&ip->ip_src, 2 * sizeof(ip->ip_src),
                            IPPROTO_UDP + (u_int32_t)ntohs(udp->uh_ulen)
                        )
                    )
                ));

	eh = &pkt->eh;

	if (dst_mac != NULL)
		bcopy(ether_aton(dst_mac), eh->ether_dhost, 6);
	else
		bcopy(ether_aton("ff:ff:ff:ff:ff:ff"), eh->ether_dhost, 6);

	if (eh->ether_dhost == NULL) {
		D("%s isn't a valid MAC address.", dst_mac);
		return -1;
	}

	if (src_mac != NULL)
		bcopy(ether_aton(src_mac), eh->ether_shost, 6);
	else
		bcopy(ether_aton("9e:27:d0:3d:b8:f7"), eh->ether_shost, 6);

	if (eh->ether_shost == NULL) {
		D("%s isn't a valid MAC address.", src_mac);
		return -1;
	}

	eh->ether_type = htons(ETHERTYPE_IP);

	return 0;
}

static void bind_dummy()
{
	int dummy_sock, rc;
	int receive_buffer;
	socklen_t opt_length = 1;
if ((dummy_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
  {
    printf ("failed to create UDP socket (%s) \n", strerror(errno));
    exit (EXIT_FAILURE);
  }
  // set the receive buffer size
  rc = getsockopt(dummy_sock, SOL_SOCKET, SO_RCVBUF, &receive_buffer, &opt_length);
  if (rc != 0)
  {
     printf ("UDP socket: could not get SO_RCVBUF (%s)\n", strerror(errno));
     exit (EXIT_FAILURE);
  }

  receive_buffer = 0;
  rc = setsockopt(dummy_sock, SOL_SOCKET, SO_RCVBUF, &receive_buffer,
                sizeof(receive_buffer));
  if (rc != 0)
  {
     printf ("UDP socket: could not set SO_RCVBUF (%s)\n", strerror(errno));
     exit (EXIT_FAILURE);
  }
  rc = getsockopt(dummy_sock, SOL_SOCKET, SO_RCVBUF, &receive_buffer, &opt_length);
  if (rc != 0)
  {
     printf ("UDP socket: could not get SO_RCVBUF (%s)\n", strerror(errno));
     exit (EXIT_FAILURE);
  }

  // bind my local (PC) port
  struct sockaddr_in pc_addr_in;
  memset(&pc_addr_in, 0, sizeof(pc_addr_in));		// Clear struct
  pc_addr_in.sin_family = AF_INET;
  pc_addr_in.sin_addr.s_addr = inet_addr("0.0.0.0"); 	// my IP address
  pc_addr_in.sin_port = htons(8192);
  rc = bind (dummy_sock, (struct sockaddr *) &pc_addr_in, sizeof(pc_addr_in));
  if (rc < 0)
  {
     printf ("could not bind UDP socket to port (%s)\n", strerror(errno));
//     exit (EXIT_FAILURE);
  }
  printf ("UDP socket created and bound to port\n");
}

void usage(char *cmd)
{
	fprintf(stderr, "Usage: \n"
			"%s arguments\n"
			"\t-i interface		interface name (rx interface for fw)\n"
			"\t-o interface		tx interface for fw (unused otherwise)\n"
			"\t-n count		number of iterations (0=inf)\n"
			"\t-R delay		delay for each burst (in us)\n"
			"\t-T pkt		packets in each burst\n"
			"\t-l len		in bytes, excluding CRC\n"
			"\t-f function		tx rx fw\n"
			"\t-b backend		linux tap netmap\n"
			"\t-d dst-ip		destination ip\n"
			"\t-s src-ip		source ip\n"
			"\t-D dst-mac		destination mac\n"
			"\t-S src-mac		source mac\n", cmd);
}

void debug_tx(struct pkt *pkt)
{
	char *src_ip, *dst_ip, *src_eth, *dst_eth;
	struct ether_addr *temp = malloc(sizeof(struct ether_addr));

	src_ip = malloc(16);
	dst_ip = malloc(16);
	src_eth = malloc(64);
	dst_eth = malloc(64);

	strcpy(src_ip, inet_ntoa(pkt->ip.ip_src));
	strcpy(dst_ip, inet_ntoa(pkt->ip.ip_dst));

	bcopy(pkt->eh.ether_shost, temp, 6);
	strcpy(src_eth, ether_ntoa(temp));

	bcopy(pkt->eh.ether_dhost, temp, 6);
	strcpy(dst_eth, ether_ntoa(temp));

	D("src ip: %s, dst ip: %s", src_ip, dst_ip);
	D("src mac: %s, dst mac: %s", src_eth, dst_eth);

	free(src_ip);
	free(dst_ip);
	free(src_eth);
	free(dst_eth);
	free(temp);
}

static inline int print_buffer_as_pkt(char *buf, size_t size)
{
	(void)size;
	debug_tx((struct pkt*) buf);

	D("Body: %s\n", ((struct pkt*)buf)->body);

	return 0;
}

static void get_stats(struct stats_t *stats)
{
	double s = ns2s(stats->ns);

	static char units[4] = { '\0', 'K', 'M', 'G' };
	double amount = 8.0 * (1.0 * stats->size * stats->count) / s;
	double pps = stats->count / s;
	int aunit = 0, punit = 0;

	while (amount >= 1000) {
		amount /= 1000;
		++aunit;
	}

	while (pps >= 1000) {
		pps /= 1000;
		++punit;
	}

	stats->pps = pps;
	stats->pps_mult = units[punit];

	stats->bw = amount;
	stats->bw_mult = units[aunit];
}

static void tx_output(struct fun_ret_t *ret_stats)
{
	struct stats_t calc_stats;
	calc_stats.size = ret_stats->pkt_size;
	calc_stats.ns = ret_stats->ns;
	calc_stats.count = ret_stats->count;

	get_stats(&calc_stats);

	printf("Sent %"PRIu64" packets, %d bytes each, in %.2f seconds.\n",
		calc_stats.count, calc_stats.size, ns2s(calc_stats.ns));

	printf("Speed: %.2f%cpps. Bandwidth: %.2f%cbps.\n",
		calc_stats.pps, calc_stats.pps_mult, calc_stats.bw, calc_stats.bw_mult);
}

static void rx_output(struct fun_ret_t *ret_stats)
{
	struct stats_t calc_stats;
	calc_stats.size = ret_stats->pkt_size;
	calc_stats.ns = ret_stats->ns;
	calc_stats.count = ret_stats->count;

	get_stats(&calc_stats);

	printf("Received %"PRIu64" packets, %d bytes each, in %.2f seconds.\n",
		calc_stats.count, calc_stats.size, ns2s(calc_stats.ns));

	printf("Speed: %.2f%cpps. Bandwidth: %.2f%cbps.\n",
		calc_stats.pps, calc_stats.pps_mult, calc_stats.bw, calc_stats.bw_mult);
}

static void fw_output(struct fun_ret_t *rx_stats, struct fun_ret_t *tx_stats)
{
	rx_output(rx_stats);
	tx_output(tx_stats);
}

static struct backend_t *backend = NULL;

struct targ_rx {
	struct open_arg_t *open_a;
	struct rx_arg_t *rx_a;
};

struct targ_tx {
	struct open_arg_t *open_a;
	struct tx_arg_t *tx_a;
};

struct targ_fw {
	struct open_arg_t *open_a;
	struct fw_arg_t *fw_a;
};

static void *rx_thread_wrapper(void *arg)
{
	struct targ_rx *targ = (struct targ_rx*) arg;

	int err = backend->open(targ->open_a, targ->rx_a->state);

	if (err) {
		D("Can't open the device %s\n", targ->open_a->dev_name);
		return 0;
	}

	backend->rx(targ->rx_a, NULL);

	return 0;
}

static void *tx_thread_wrapper(void *arg)
{
	struct targ_tx *targ = (struct targ_tx*) arg;

	int err = backend->open(targ->open_a, targ->tx_a->state);

	if (err) {
		D("Can't open the device %s\n", targ->open_a->dev_name);
		return 0;
	}

	backend->tx(targ->tx_a);

	return 0;
}

static struct fw_arg_t *fw_arg = NULL;
static void *tx_state = NULL;

static int forwarder(int packet_count, void *state)
{
	static struct fun_ret_t tx_ret;
	static struct tx_arg_t arg;

	arg.pkt_size = fw_arg->pkt_size;
	arg.pkt_in_burst = fw_arg->pkt_in_burst;
	arg.count = packet_count;
	arg.master_pkt = fw_arg->master_pkt;
	arg.rem = fw_arg->rem;
	arg.ret = &tx_ret;
	arg.state = tx_state;
	backend->tx(&arg);

	return 0;
}


static int fw(struct fw_arg_t *arg)
{
	struct open_arg_t open_a;
	struct fun_ret_t *rx_ret = arg->rx_ret;
	struct fun_ret_t *tx_ret = arg->tx_ret;

	memset(tx_ret, 0, sizeof(struct fun_ret_t));
	tx_ret->pkt_size = arg->pkt_size;

	open_a.flag = OPEN_TX;
	open_a.dev_name = arg->tx_dev_name;

	tx_state = (void*) backend->getcore();

	if (backend->open(&open_a, tx_state) != 0) {
		D("Can't open device %s for tx", arg->tx_dev_name);
		return 1;
	}

	fw_arg = arg;

	struct rx_arg_t local_rx_a;
	struct fun_ret_t local_rx_ret;
	local_rx_a.count = arg->count;
	local_rx_a.pkt_in_burst = arg->pkt_in_burst;
	local_rx_a.ret = &local_rx_ret;
	local_rx_a.state = arg->state;

	backend->rx(&local_rx_a, forwarder);

	tx_ret->ns = rx_ret->ns = local_rx_ret.ns;
	rx_ret->count = local_rx_ret.count;
	tx_ret->pkt_size = rx_ret->pkt_size = local_rx_ret.pkt_size;

	backend->close(tx_state);
	free(tx_state);
	return 0;
}

static void *fw_thread_wrapper(void *arg)
{
	struct targ_fw *targ = (struct targ_fw*) arg;

	int err = backend->open(targ->open_a, targ->fw_a->state);

	if (err) {
		D("Can't open the device %s\n", targ->open_a->dev_name);
		return 0;
	}

	fw(targ->fw_a);
	fw_output(targ->fw_a->rx_ret, targ->fw_a->tx_ret);

	return 0;
}

#define MAX_IF 50


/* control-C handler */
static void sigint_h(int sig)
{
	(void)sig;      /* UNUSED */
	exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
	int ch, i, nif = 0;
	char *func = NULL;
	char *src_ip, *dst_ip, *src_eth, *dst_eth;

	struct open_arg_t open_a[MAX_IF];


	struct fw_arg_t fw_a[MAX_IF];
	struct rx_arg_t rx_a[MAX_IF];
	struct tx_arg_t tx_a[MAX_IF];

	struct timespec rem;
	struct pkt *pkt = malloc(sizeof(struct pkt));

	rem.tv_sec = 0;
	rem.tv_nsec = 0;
	tx_a[0].rem = fw_a[0].rem = &rem;
	tx_a[0].pkt_in_burst = rx_a[0].pkt_in_burst = fw_a[0].pkt_in_burst = 200;

	fw_a[0].pkt_size = tx_a[0].pkt_size = 64;
	tx_a[0].count = fw_a[0].count = rx_a[0].count = 0;
	fw_a[0].tx_dev_name = NULL;
	src_ip = dst_ip = src_eth = dst_eth = NULL;

	signal(SIGINT, sigint_h);

	while ( (ch = getopt(argc, argv, "o:i:n:f:b:s:d:S:D:R:T:")) != -1 ) {
		switch(ch) {
		default:
			D("bad option %c %s", ch, optarg);
			usage(argv[0]);
		case 'i':
			open_a[nif].dev_name = optarg;
			++nif;
			break;
		case 'o':
			fw_a[0].tx_dev_name = optarg;
			break;
		case 'n':
			rx_a[0].count = tx_a[0].count = fw_a[0].count = atoi(optarg);
			break;
		case 'f':
			func = optarg;
			if (!strncmp(func, "rx", 2) || !strncmp(func, "fw", 2)) {
				open_a[0].flag = OPEN_RX;
			} else if (!strncmp(func, "tx", 2)) {
				open_a[0].flag = OPEN_TX;
			}
			break;
		case 'b':
			if (!strcmp("netmap", optarg)) {
				backend = &netmap_backend;
			} else if (!strcmp("linux", optarg)) {
				backend = &linux_backend;
			} else {
				D("bad option %s, must be linux or netmap", optarg);
				return EXIT_FAILURE;
			}
			break;
		case 'd':
			dst_ip = optarg;
			break;
		case 's':
			src_ip = optarg;
			break;
		case 'D':
			dst_eth = optarg;
			break;
		case 'S':
			src_eth = optarg;
			break;
		case 'l':
			tx_a[0].pkt_size = atoi(optarg);
			break;
		case 'R':
			tx_a[0].rem->tv_nsec = atol(optarg) * 1000L;
			break;
		case 'T':
			tx_a[0].pkt_in_burst = rx_a[0].pkt_in_burst = fw_a[0].pkt_in_burst = atoi(optarg);
			break;
		}
	}

	if (backend == NULL) {
		D("No backend (or erroneus backend) selected.\n");
		return EXIT_FAILURE;
	}

	if (open_a[0].dev_name == NULL) {
		D("No ifname specified.\n");
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	if (func == NULL) {
		D("No function specified.\n");
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	initialize_packet(pkt, tx_a[0].pkt_size, src_ip, dst_ip, src_eth, dst_eth);
	tx_a[0].master_pkt = pkt;
	fw_a[0].master_pkt = pkt;

	for (i=1; i<nif; i++) {
		open_a[i].flag = open_a[0].flag;
	}

	struct fun_ret_t ret[MAX_IF*2];
	struct targ_rx t_arg_rx[MAX_IF];
	struct targ_tx t_arg_tx[MAX_IF];
	struct targ_fw t_arg_fw[MAX_IF];
	char *states[MAX_IF];
	pthread_t tid[MAX_IF];

	memset(&ret, 0, sizeof(struct fun_ret_t)*MAX_IF*2);
	if (backend == &linux_backend && !strncmp(func, "rx", 2))
		bind_dummy();

	for (i=0; i<nif; i++) {
		states[i] = backend->getcore();

		if (states[i] == NULL) {
			D("Null state. Exiting");
			return EXIT_FAILURE;
		}


		if (!strncmp(func, "rx", 2)) {
			//backend->rx(&rx_a, &ret, state, print_buffer_as_pkt);
			D("Receiving from %s", open_a[i].dev_name);

			memcpy(&rx_a[i], &rx_a[0], sizeof(struct rx_arg_t));
			rx_a[i].ret = &ret[i];
			rx_a[i].state = states[i];

			t_arg_rx[i].rx_a = &rx_a[i];
			t_arg_rx[i].open_a = &open_a[i];

			pthread_create( &tid[i], NULL, rx_thread_wrapper, (void*) &t_arg_rx[i]);

		} else if (!strncmp(func, "tx", 2)) {
			D("Transmitting on %s, packet of %d byte", open_a[i].dev_name, tx_a[0].pkt_size);

			memcpy(&tx_a[i], &tx_a[0], sizeof(struct tx_arg_t));
			tx_a[i].ret = &ret[i];
			tx_a[i].state = states[i];
			t_arg_tx[i].tx_a = &tx_a[i];
			t_arg_tx[i].open_a = &open_a[i];

			pthread_create(&tid[i], NULL, tx_thread_wrapper, (void*) &t_arg_tx[i]);

		} else if (!strncmp(func, "fw", 2)) {
			if (fw_a[0].tx_dev_name == NULL) {
				D("No tx dev specified.\n");
				usage(argv[0]);
				return EXIT_FAILURE;
			}

			memcpy(&fw_a[i], &fw_a[0], sizeof(struct tx_arg_t));
			fw_a[i].tx_ret = &ret[i];
			fw_a[i].rx_ret = &ret[i*2];

			fw_a[i].state = states[i];

			t_arg_fw[i].open_a = &open_a[i];
			t_arg_fw[i].fw_a = &fw_a[i];

			pthread_create(&tid[i], NULL, fw_thread_wrapper, (void*) &t_arg_fw[i]);
		} else {
			D("Unsupported function %s.\n", func);
			usage(argv[0]);
			return EXIT_FAILURE;
		}
	}

	struct fun_ret_t total_ret;
	total_ret.ns = 0;
	total_ret.count = 0;
	total_ret.pkt_size = 0;

	for (i=0; i<nif; i++) {
		pthread_join(tid[i], NULL);

		if (ret[i].ns > total_ret.ns)
			total_ret.ns = ret[i].ns;
		total_ret.count += ret[i].count;
		total_ret.pkt_size = ret[i].pkt_size;

		backend->close(states[i]);
		free(states[i]);
	}

	free(pkt);

	if (!strncmp(func, "rx", 2)) {
		rx_output(&total_ret);
	} else if (!strncmp(func, "tx", 2)) {
		tx_output(&total_ret);
	}

	return EXIT_SUCCESS;
}
